# importing openpyxl
from openpyxl import Workbook

# importing date and time
import datetime

# importing csv file API (native python module)
import csv

# importing sys for exception handling
import sys


def openFile():
	"""
	function responsible for inputting file name from user and openning it.
	there are checks to be done:
	- is the file csv?
	- does the file exist?
	Input  : void
	output : file handler
	"""
	fileName = input("enter file name (.csv file): ");
	if fileName[-4:] != ".csv":
		fileName += ".csv"
	try:
		handler  = open(fileName, "r")
		return handler
	except Error:
		print("error: " + Error)
		return null


def processFile(fh):
	"""
	processFile is responsible for operating on csv file and change it to xlsx file
	Input  : csv file handler
	Output : xlsx matrix to save
	"""
	xlsx = []
	tmxlsx = []
	colDic = []
	try:
		colCount = -1
		csvReader = csv.reader(fh)
		csv.list_dialects()
		for row in csvReader:
			tmxlsx.append(row)
			colCount = max(colCount, len(row))
	finally:
	    fh.close()

	try:
		rowNum = 0
		firstRow = 0
		for row in tmxlsx:
			if len(row) < colCount:
				continue
			if firstRow == 0:
				colDic = row
				firstRow = 1
				continue
			
			xlsx.append([0 for x in range(colCount)])
			
			counter = 0
			for col in row:
				if len(col) == 0:
					break
				if colDic[counter] == "Date" or colDic[counter] == "date":
					dateStr = datetime.datetime.strptime(col, '%Y/%m/%d').strftime('%A') + ", " + \
					datetime.datetime.strptime(col, '%Y/%m/%d').strftime('%B') + " " + \
					datetime.datetime.strptime(col, '%Y/%m/%d').strftime('%d') + ", " + \
					datetime.datetime.strptime(col, '%Y/%m/%d').strftime('%Y')
					xlsx[rowNum][counter] = dateStr
				elif colDic[counter] == "Time" or colDic[counter] == "time":
					timeStr = datetime.datetime.strptime(col, '%H:%M:%S').strftime('%H') + ":" + \
					datetime.datetime.strptime(col, '%H:%M:%S').strftime('%M') + ":" + \
					datetime.datetime.strptime(col, '%H:%M:%S').strftime('%S')
					xlsx[rowNum][counter] = timeStr
				else:
					if col.find("E") != -1:
						xlsx[rowNum][counter] = format(float(col), '.2f')
					else:						
						xlsx[rowNum][counter] = int(col)

				counter += 1

			rowNum += 1
	except:
		print("error: ", sys.exc_info()[0])
		return false
	
	return xlsx

def saveXlsx(xlsx):
	"""
	save the csv content read in xlsx matrix to xlsx file
	Input  : xlsx matrix
	Output : true on success or false otherwise
	"""
	fileName = input("enter file name of the file to be saved (.xlsx file): ")
	if fileName[-5:] != ".xlsx":
		fileName += ".xlsx"
	
	wb = Workbook()
	ws = wb.active
	ws.title = fileName
	try:
		for row in xlsx:
			ws.append(row)

		wb.save(fileName)	
		return True
	except:
		print("error: ", sys.exc_info()[0])
		return False


# TODO: should implement interact mode
# fh   = openFile()
# xlsx = processFile(fh)
# saveXlsx(xlsx)

