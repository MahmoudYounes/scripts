#!/bin/bash

if [ "$(whoami)" != "root" ]
	then
	echo "need to be root to execute this script"
	exec  sudo -- "$0" "$@"
fi

echo "enter directory name to reserve files (/var/www/html/filename): "
read DIR_NAME

[[ -d "/var/www/html/$DIR_NAME" ]] || mkdir /var/www/html/$DIR_NAME


cp -r ./* /var/www/html/$DIR_NAME
if [ $? -ne 0 ] 
then
	echo "error in copying files"
	exit
fi

chmod 777 /var/www/html/$DIR_NAME/*
if [ $? -ne 0 ] 
then
	echo "script finished with erros"
	exit
fi
echo "script finished successfully"
exit
