<?php

require_once("functions.php");

if(isset($_POST['sndrid']) && isset($_POST['rcvrid']) && isset($_POST['msgContent']))
{
	$sndrid = $_POST['sndrid'];
	$rcvrid = $_POST['rcvrid'];
	$content = $_POST['msgContent'];

	if(isset($_POST['trans']))
	{
		$rcvrid = sender_id($rcvrid)[0]['id'];
		if(!isset($rcvrid))
		{
			echo "error no such account";
			exit;
		}		
	}

	$message = send_message($sndrid, $rcvrid, $content);

	if(isset($_POST['trans']) && $message)
		echo $rcvrid;
	else if($message)
		echo "success";
	else
		echo "error: failed";
	exit;
}
else
{
	echo "error in post";
	exit;
}


?>