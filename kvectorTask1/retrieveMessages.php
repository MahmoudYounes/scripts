<?php 

	require_once("functions.php");
	
	
	if(isset($_POST['sndrid']) && isset($_POST['rcvrid']))
	{
		$sndrid = $_POST['sndrid'];
		$rcvrid = $_POST['rcvrid'];
		
		$messages = fetch_messages_from_to($sndrid, $rcvrid);

		echo json_encode($messages);	
		exit;
	}
	else
	{
		echo "error";
		exit;
	}

?>