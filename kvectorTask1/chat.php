<?php

/**
 * kchat file logic goes here. this should allow us to chat together. note that part of the logic here depends on
 * you setting a session at signin function (in functions.php). you learn how to set a session because this is an 
 * important thing in websites development. also I would recommend you learn how to deal with cookies
 * (check out testSession.php and testCookies.php)
 *
 * note how we change HTML output based on php conditions!
 */


require_once("functions.php");

// if you do not set $_SESSION['id'] in signin function please go and set it otherwise we will be redirected to index.php
// to signin.
if (!isset($_SESSION['id']))
{
	redirect_to("index.php");
}


// $_SESSION array is very useful in carrying messages from one page to another. have you ever encountered a website
// that gives you a shopping cart while you navigate through pages adding products to it?. most likely they implement
// shopping carts using $_SESSION array.

$accountid = $_SESSION['id'];    				// we will use this to identify the user we are dealing with
$accountSenders = fetch_senders($accountid);	// these are the accounts who sent us messages (Implemented)


?>
<html>
<head>
	<title>kchat - chat</title>
	<script type="text/javascript" src="jquery-3.1.0.js"></script>
<style>
html
{
	background-color: #F3F2F2;
	color 			: #000000;	 			
}
.functions
{
	width 			: 100%;
	height  	  	: 5%;
	float 			: left;
	display 	    : block;

}
.container
{
	width			: 100%;
	height  		: 100%;
	display 	    : block; 
}
.messagesSent
{
	width  	    	: 29%;
	height       	: 100%;   
	float 			: left;
	display 	    : block;
	border-right-style    : solid;
	border-right-width    : 1%;
	border-right-color 	  : grey; 

}
.chatArea
{
	display 	    : block;
	width 			: 70%;
	height 		    : 100%;
	float       	: left;
}
.messageChatArea
{
	width 	    	: 100%;
	height  	    : 80%;
}
.sendMessage
{
	display 	    : block;
	width 	    	: 100%;
	height  	    : 20%;
	float 			: left;
}
.messageSenders
{
	display 		: block;
	width			: 100%;
	float 	  	    : left;
	height  	 	: 10%;
}
.message1
{
	display 	    : block;
	float			: left;
	background-color: white;
	width 			: 100%;
}
.message2
{
	display 	    : block;
	float			: left;
	text-align      : right;
	background-color: grey;
	width 			: 100%;
}
</style>

</head>
<body>
	<!--
		our main container
	-->
	<div class="container"
		<!--
			our functions bar
		-->
		<div id="functions" class="functions">
			<button onclick="newMessage()">new message</button>
			<button onclick="logOut()">log out</button>
		</div>
		<hr/>
		<!--
			here are all the messages that reached us
		-->
		<div class="messagesSent">
			<?php if (!is_array($accountSenders)) {?>
			<div class="messageSenders" onclick="viewMessage(<?php echo $accountSenders ?>)">
				<span style="text-align: center;"><?php echo sender_name($accountSenders); //check out sender_name function ?></span><br/>
			</div>
			<?php } else { foreach($accountSenders as $sndr) {?> 

			<div class="messageSenders" onclick="viewMessage(<?php echo $sndr['senderID'] ?>,true)">
				<span style="text-align: center;"><?php echo sender_name($sndr['senderID'])[0]['name']; //check out sender_name function ?></span><br/>
			</div>
			<hr/>
			<?php }} ?>
		</div>
		<!--
			when pressing on message, they get opened in the chat area
		-->
		<div class="chatArea">
			<!--
				where the messages get displayed
			-->
			<div id="messageChatArea" class="messageChatArea">
				
			</div>
			<!--
				where we write messages and send
			-->
			<div class="sendMessage">
				<textarea id="messageToSend" style="width:100%; height:70%;" row="100" col="50" name="messages"></textarea>
				<button onclick="sendNewMessage()">Send</button>
			</div>
		</div>

	</div>
	<script type="text/javascript">
	// this variable holds the active ID (the id of the messages viewed in the message area)
	var activeID = -1;
	var rcvr = <?php echo $_SESSION['id']?>;
	var path = 1;

	// the functions written here are written using AJAX 
	// I should go ask them if they want to learn something 
	// like AJAX and then see if it should be required or no.
	function sendNewMessage()
	{
		console.log(path);
		if(path == 1)
		{
			msgprt = document.getElementById("messageToSend");
			var message = msgprt.value;
		    msgprt.value = "";

			if(activeID < 0)
			{
				msgprt.value = "must pick an account to send message first";
				return;
			}
		    $.ajax({
				type: "post",
				url: "sendMessage.php", 
				data: {'sndrid':rcvr, 'rcvrid':activeID, 'msgContent':message},
				success: function(result){
				 	if(result != "success")
				 	{
				 		window.alert(result);
				 	}

				}
			});
		}
		else
		{
			activeID = document.getElementById("inputAcc").value;
			if(!activeID)
			{
				window.alert("invalid account");
			}
			
			msgprt = document.getElementById("messageToSend");
			var message = msgprt.value;
		    msgprt.value = "";

			$.ajax({
				type: "post",
				url: "sendMessage.php", 
				data: {'sndrid':rcvr, 'rcvrid':activeID, 'msgContent':message, 'trans':"true"},
				success: function(result){
				 	if(result != "failed")
				 	{
				 		activeID = result;
				 	}

				}
			});

			path = 1;
			child = document.getElementById("label");
			child.parentNode.removeChild(child);

			child = document.getElementById("inputAcc");
			child.parentNode.removeChild(child);

			viewMessage(activeID);

		}
	}

	function newMessage()
	{
		activeID = -1;
		chatarea = document.getElementById("messageChatArea");
		chatarea.innerHTML = "";

		functionsArea = document.getElementById("functions");

		label = document.createElement("label");
		label.id = "label";
		t = document.createTextNode("targetAccount:  ");
		label.setAttribute("for", "targetAccount");
		label.appendChild(t);


		functionsArea.appendChild(label);


		msgAcc = document.createElement("input");
		msgAcc.id = "inputAcc"
		msgAcc.type = "text";
		msgAcc.class = "inputAccName";

		functionsArea.appendChild(msgAcc);
		path = 0;

	}


	function viewMessage(id, clearDiv = false)
	{
		var div = document.getElementById("messageChatArea");
		
		div.innerHTML = "";
		

		
		$.ajax({
			type: "post",
			url: "retrieveMessages.php", 
			data: {'rcvrid':rcvr, 'sndrid':id},
			success: function(result){
				if(result)
				{
					if(result.indexOf("error") !== -1)
					{
						console.log(result);
						window.alert(result);
						return;
					}

					ret = JSON.parse(result);
					
					if(typeof ret == "object"){
						ret.forEach(function(res){
							var newmsg = document.createElement('div');
							newmsg.innerHTML = res["content"];
							if(res["recieverID"] == rcvr)
								newmsg.className = "message1";
							else
								newmsg.className = "message2";
							div.appendChild(newmsg);
						});
					}
					else
						return;
					activeID = id;
				}
				else
				{
					window.alert("error");
				}
	   		}
	   		
		});
		
		
	}

	function logOut()
	{
		window.location = "logout.php";
	}

	setInterval(function(){
		
		// if no account is active return
		if(activeID < 0) return;
		viewMessage(activeID, false);

	},2000);

	</script>
</body>
</html>
