<?php
/**
 * In this file we test the GET submission method to show you that get data is visible in the
 * URl.
 *
 * you access this file through localhost/testGET.php
 *
 */

?>

<html>
<head>
	<title>test get submission</title>
	<style>
	.submissionData
	{
		min-width: 60px;
	}
	form
	{
		width: 100%;
		min-height: 22em;
		text-align:center;
	}
	</style>
</head>
<body>
	<div class="submissionData">
	<?php
		// recall: we can write php anywhere in the page, and the page gets executed sequential 
		// from top to bottom.
		$output  = "now try submitting some data other than the ones in the form. <br/>";
		$output .= "for example try pressing this url ";
		$output .= "<a href=\"localhost/testGET.php?third=Hello&fourth=world\">localhost/testGET.php?first=Hello&second=world&submit=yes</a>";
		if(isset($_GET['submit']))
		{
			echo "<pre>";
			print_r($_GET);
			echo "</pre>";
		}
		echo $output;
	?>
	</div>
	<form method="GET" action="testGET.php">
		<!-- 
			note that the attribute name is the name with which this input appears in the 
			GET/POST array. try changing the attribute name for example to see the effect of this in the GET ARRAY
		-->
		<label>first name</label>
		<input type="text" name="fname"/>
		<label>second name</label>
		<input type="text" name="sname"/>
		<label>password</label>
		<input type="password" name="password"/>
		<input type="submit" name="submit" value="submit"/>
	</form>
</body>
</html>
