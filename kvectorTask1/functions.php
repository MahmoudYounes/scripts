<?php

/*
|-------------------------------------------------------------------------------
| functions.php
|-------------------------------------------------------------------------------
| this file contains every thing that will help us to create our chat system.
| It requires and includes everything required by all pages so that we do    
| not have to bother about anything while working. enjoy implementing    
|
*/


// MySql database connection arguments.
// you may want to search for: - how to connect to mysql database from php
define("DB_SERVER", 'localhost');	// the domain of the database (where the database is)
define("DB_USER", 'root');			// database user
define("DB_PASS", 'pD123456'); 		// password of root should go here
define("DB_NAME", 'kchat'); 		// this is the database name 


// create database connection if none is found
if(!isset($db) || $db == null)
{
	$db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
}


// start a session if none is started
if (session_status() == PHP_SESSION_NONE) 
{
    session_start();
}


/**
 * Implemented for you.
 * redirects the user to another page through injecting location url in header
 * @var $location string: the location of page to redirect to.
 *
 */
function redirect_to($location)
{
	header("Location: " . $location);
	exit;
}


/**
 * Required
 * signup a user. return true if sign up successful, false otherwise.
 * @var $username string: the name of the user.
 * @var $email string: the email of the user.
 * @var $password string: the password of the user.
 *
 */
function signup($username, $email, $password)
{
	global $db;
	// TODO: create the query which should look like this.
	// INSERT INTO accounts(`name`, `email`, `password`) VALUES ('$username', '$email',
	// '$password');
	// bonus for a galaxy chocolate: escape values $username, $email, $password.
	// why do w/e escape values?


	// TODO: call the queryDB function. give it the query you created, store results (if any)
	// in a variable.
	
	
	// TODO: check if the insertion (query) was done correctly or no.
	
	return $results; //remove this after you finish implementation.

	// TODO: after you finish this function proceed to signin.php page
}


/**
 * Required
 * check if user exists, then sign him in. otherwise return false.
 * @var username string: the username of the user signing in
 * @var password string: the password of the user signing in
 *
 */
function signin($username, $password)
{
	global $db;
	// TODO: create the query which should like this.
	// SELECT * FROM `accounts` WHERE name=$username and password=$password;
	// bonus for a galaxy chocolate: escape values $username, $password.
	// you might want to search for : 
	// - escaping user input data in php
	// - avoid sql injection in php
	// why do we escape values?

	// TODO: call the queryDB function which is implemented for you. give it the query you
	// created, and store results (if any) in a variable.
	

	// TODO: check for the returned results, if they are not null then we have an 
	// authenticated user, otherwise, this user does not exist.
	// TODO: set a session indicating that there is a signed in user. let the session variable be id
	// i.e, you should say $_SESSION['id'] = <id you retrieved from database>
	// what is a session? and how can we deal with it?
	
	return false; //remove this after you finish implementation.

	// TODO: after finishing this page proceed to chat.php page to implement the chatting functionality
}


/**
 * Required (do not implement for now)
 * this function fetchs all account senders who sent us messages
 * @var id : this should be the id of the reciever account 
 * @var returnedaccounts : the set of accounts who messaged us
 *
 */
/*function fetch_messages_to($id)
{
	global $db;
	// TODO: escape inputs

	// TODO: setup the query that should look like this: 
	// SELECT * FROM `messages` WHERE `recieverID` = $id;
	
	
	// TODO: call queryDB with the query you prepared in the above part and store results returned from this function

	// TODO: check on results. if results exist return them otherwise return false

	return false; // TODO: remove this after you finish your code
}*/


/**
 * Required
 * this function fetchs all messages sent from an account
 * @var id : this should be the id of the sender account 
 * @var returnedMessages : the set of messages returned from database
 *
 */
function fetch_messages_from_to($sndrid, $rcvrid)
{
	global $db;
	// TODO: escape inputs


	// TODO: setup the query that should look like this: 
	// SELECT * FROM `messages` WHERE senderID = $sndrid and recieverID = $rcvrid 
	// order by sent_at 
	
	// TODO: call queryDB with the query you prepared in the above part and store results returned from this function

	// TODO: check on results. if results exist return them otherwise return false
	return false;

}


/**
 * Implemented for you as an example to refer to
 * this function fetchs all the people who sent us messages from the database
 * @var id : this should be the id of the reciever account
 * @var results : this should be the ids of the poeple who sent us messages
 *
 */
function fetch_senders($id)
{
	global $db;
	// escape input
	$safe_id = mysqli_escape_string($db, $id);

	// prepare query
	$query  = "SELECT DISTINCT senderID ";
	$query .= "FROM messages ";
	$query .= "WHERE recieverID={$safe_id};";

	// execute query and get results
	$results = queryDB($query);

	// check on results
	if(!$results)
	{
		die("error executing query");
	}


	//return the results
	return $results;
}


/**
 * Implemented for you
 * this function retrieves the name corresponding to a certain id 
 * @var id (int) : id of account
 * @var name(string) : username of the account
 *
 */
function sender_name($id)
{
	global $db;
	// escaping id for sql injection
	

	// prepare query that should look somethin like:
	// SELECT * FROM accounts WHERE id=$safe_id LIMIT 1;

	// executing query

	// checking $results

	//returning the results
	return false; // edit this line to return correct results
}


/**
 * Required to be Implemented
 * this function query the db to place a message sent from sender to reciever.
 * @var sndrID (int): the id of the sender
 * @var rcvrID (int): the id of the reciever
 * @var content (string): msg content
 *
 */
function send_message($sndrID, $rcvrID, $content)
{
	global $db;
	
	// TODO: escape arguments to be safe

	// TODO: prepare query. should look something like this:
	// INSERT INTO messages (senderID, recevierID, content, time) VALUES ( and you know the rest)
	// use the NOW() function to get the current time of message from database

	// TODO: call queryDB on your query

	// TODO: check returned results

	//TODO: return results
	return false; // edit this line to return correct results


}


/**
 * Implemented for you
 * this function retrieves sender id given username
 * @var name(string): username
 *
 */
function sender_id($name)
{
	global $db;

	$safe_name = mysqli_escape_string($db, $name);

	$query  = "SELECT id ";
	$query .= "FROM accounts ";
	$query .= "WHERE name='{$safe_name}' ";
	$query .= "LIMIT 1;";

	
	$results = queryDB($query);

	if(!$results)
	{
		die("error in database");
	}

	return $results;

}


/**
 * Implemented for you, but please read and try to understand carefully
 * open connection with database, executes query, return the results if exist or true/false
 * @var query string: the query to execute
 */
function queryDB($query)
{
	global $db;

	// execute the query. mysqli_query is a built in function in php that is used to execute queries to db connection
	$results = mysqli_query($db, $query);

	// confirm that no error happened while executing the query.
	if (!$results) 
	{
		return false;
	}

	// change the results (if any) to associative array.
	// mysqli_fetech_assoc is a built in function in php, check the php documentation through google
	$output = array();
	while($row = mysqli_fetch_assoc($results))
	{
		$output[] = $row;
	}

	// if the number of rows fetched from the query is bigger than zero (i.e, we have rows
	// returned from the query which was a SELECT statement) then we will return these rows,
	// otherwise this was a statement like (UPDATE, INSERT, DELETE, etc.) so if successful 
	// return true, if not successful the check on results would have been null and we would
	// have returned false from the begining.
	if(count($output) > 0)
	{
		return $output;
	}
	else
	{
		return true;
	}

	//now we close the connection.
	mysql_close($db);
	unset($db);
}



?>