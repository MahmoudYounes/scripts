|********************************************************************************************|
|                             		  TASK 1: CHAT SYSTEM        	   	 			         |
|********************************************************************************************|


# What you will learn in this task

	- you will learn how to construct a database through MySQL command line or phpmyadmin(recommended)
	- you will learn how to submit requests to a server and process these requests using a back-end programming language such as php
	- you will learn how to deal with html/css/php together as a whole through a form.


# TASK Pre-Requistes
	
	- basic understanding of php syntax.
	- basic understanding of MySQL databases Syntax.
	- HTML/CSS.
	- (optional) JavaScript.
	- pick up a good text editor (recommended: sublime text editor)(others: dreamweaver, notepad++, ATOM are good choices as well)


# How to make things work
	
	- it is preferable to install wamp directly under C:\ (windows partition)
	- on windows after installing WAMP stack, copy then paste the task1 directory into the www directory inside the directory of WAMP.
	i.e: copy task1 into C:\wamp\www\
	- start wamp then open your prefered browser and type:
		localhost/task1/index.php


# structure of the task
	
	- README.rd    : this read me file.
	- index.php    : main page of the chatting system (welcome page. this is where you should start, it contains insturctions on how to proceed in the task).
	- signup.php   : page for new comers to signup.
	- signin.php   : page for people to login to their accounts.
	- functions.php: this file contains all php functions that we will need.
	- chat.php     : main page for the chatting functionality.
	- testGET.php  : php page only to get you familiar with GET method.
	- kchat.sql    : sql file that contains sql queries to create your database.

# further notes:
	
	- every required function will be documented with the steps to do in this function.