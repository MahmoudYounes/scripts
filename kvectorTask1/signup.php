<?php

/*
|----------------------------------------------------------------------
| signup.php
|----------------------------------------------------------------------
| this is the php page for creating a user into our system then
| redirecting them to the signin.php page to sign in and chat.    
| note: that the form is processed in the same page as it was
| written and the url to which we submitted the form is this 
| page that we coded in php. lots of logic implemented here
| 
| 
*/	



// this is like #include <iostream> for example. it includes a file in here. the functions.php file has all our required functions
require_once("functions.php");



if(isset($_POST['submit']))
{
	// TODO: prepare to call sign up function. function should take username, email and password and return true if sign up successful or false if an error occured.

	// recognize this from c/c++ ?
	// if not, search for shorthand if/else in php or shorthand if/else for c++.
	$username   = (isset($_POST['username']))? $_POST['username'] : die("bad username.");
	$password   = (isset($_POST['password']))? $_POST['password'] : die("bad password.");
	$repassword = (isset($_POST['repassword']))? $_POST['repassword'] : die("passwords does not match.");
	$email = (isset($_POST['email']))? $_POST['email'] : die("bad email."); 
	


	//TODO: implement server side checks here. (Required)
	$valid = true; 			// will be used to store validity of the data
	// - check that password = the retyped password (will be implemented as example)
	$valid = $valid && ($password == $repassword); 
	// - check that name is less than 60 chars
	// - check that password is less than 60 chars
	// - check that email is less than 60 chars


	// we AND the $valid variable with the checks we want. if something is violated
	// ANDing it with valid will make valid false and will make us enter the if condition
	if(!$valid) // if something is not valid, die.
	{
		die("you entered wrong data. refresh please.");
	}

	
	// otherwise,
	// if signup returned true then redirect to signin.php
	// go to functions.php to implement signup function
	if (signup($username, $email, $password))
	{
		redirect_to("signin.php");
	}
	else
	{
		echo "<p style=\"text-align:center;\">failed to sign you up, please retry.</p><br/>";
	}
}



?>

<html>
<head>
<title>kchat - signup</title>
<style>
form
{
	text-align: center;
	width: 100%;
	min-height: 22em;
}
table
{
	margin-left: 40%;
}
</style>
</head>
<body>
<div>
	<a href="index.php">&lt;&lt; back</a>
</div>
	<form method="POST" action="signup.php">
		<table>
			<tr>
				<td><label for="username">username</label></td>
				<td><input type="text" name="username"></td>
			</tr>
			<tr>
				<td><label for="password">password</label></td>
				<td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td><label for="repassword">retype password</label></td>
				<td><input type="password" name="repassword"></td>
			</tr>
			<tr>
				<td><label for="email">email</label></td>
				<td><input type="email" name="email"></td>
			</tr>
			<tr>
				<td><input type="submit" name="submit" value="submit"></td>
			</tr>
		</table>
	</form>
</body>
</html>

<!-- 
	after finishing this page correctly, proceed to signin.php
-->
