<?php
	
/**
 * this is the main page of our chatting system. here you will find code for the UI of
 * our welcome page. you will also find instructions on how to proceed with this task and
 * what to do and implement.
 *
 * you might want to search for the following on google:
 * - why is it important to link website with database
 * - how to develop website using php and mysql
 * 
 * keep searching until you feel you grasped the intution of why we should use databases
 * with website.
 *
 *
 * first thing first: you need to create the database. we will use MySQL database for 
 * this task. first open phpmyadmin by visiting this url localhost/phpmyadmin 
 * (do not forget to run wamp server before doing so).
 * if phpmyadmin is installed it will give you an interface to login with your user
 * credentials you used for mysql database.
 * (if stuck in this step, use google with the following search query: 
 * - how to set up password for root user in mysql
 * - how to log on to phpmyadmin
 * - how to log on to mysql command line on windows
 * (Remember: google is your best friend in your life. so if there is something you do 
 * do not understand make sure to ask google or me. eventually I will go ask google 
 * on your behalf :D)
 * 
 *
 * our database will look like this: (database name: kchat)
 *
 * table: accounts
 *  -----------------------------------------------------------------------------------
 * | +id (integer(10)(auto increment)(primary key))					     			   |
 *  -----------------------------------------------------------------------------------
 * | +name (varchar(60))                                                               |
 *  -----------------------------------------------------------------------------------
 * | +email (varchar(60) unique)                                                       |
 *  -----------------------------------------------------------------------------------
 * | +password (varchar(60))									                       |
 *  -----------------------------------------------------------------------------------
 *
 *
 * table: messages
 *  -----------------------------------------------------------------------------------
 * | +id (integer(10)(auto increment)(primary key))									   |
 *  -----------------------------------------------------------------------------------
 * | +senderID (Integer(10) foreign key referencing id of table accounts)              |
 *  -----------------------------------------------------------------------------------
 * | +recieverID (Integer(10) foreign key referencing id of table accounts)            |
 *  -----------------------------------------------------------------------------------
 * | +message content (longtext)                                                       |
 *  -----------------------------------------------------------------------------------
 * | +message date (datetime)                                                          |
 *  -----------------------------------------------------------------------------------
 *
 * note: messages will be sorted in view based on their id, since id is auto incremented
 * by database. 
 *
 * 
 * to create these tables you have two ways, first way (easy & recommended) go through 
 * phpmyadmin interface and create a database and add tables to it and so on. 
 * this process is easy due to the easy and expressive interface of phpmyadmin.
 * there are lots of resources on the internet explaining how to deal with phpmyadmin to
 * create databases and tables.
 *
 * the second way is to use mysql commandline and write sql commands to create the
 * database and tables. this is the geeks way, because normally when you want to deploy
 * your website on a server, there is a greate chance that you will not find GUI or
 * desktop to work from, you will only have access to command line (cmd in
 * windows for example).
 *
 * for the first way i will let you go through the internet and search, if stuck inbox me.
 * for the second way check kchat.sql file.
 * it will be a nice challenge if you created the above database and tables using sql
 * on your own, and you will learn alot from it.
 * you might want to search for the following on google:
 * - how to open mysql command line on windows
 *
 *
 * as you can see in this page, we are only welcoming the user and redirecting them to
 * either login or sign up page. so nothing to be done here. note that our UI will be 
 * very simply, no greate CSS, however, you are free to change the CSS the way you 
 * want.
 *
 * now head to signup.php as your next stop.
 *
 */

?>

<html>
<head>
	<title>index page</title>
	<style>
	</style>
</head>
<body>
<div class="headerDiv">
	<h2 style="text-align:center">WELCOME TO KVECTORIANS CHAT</h2>
	<br/>
	<p style="text-align:center">if you have an account procceed to sign in. otherwise create an account using sign up</p>
	<br/>
	<p style="text-align:center"><a href="signin.php">Sign In</a></p>
	<p style="text-align:center"><a href="signup.php">Sign Up</a></p>
</div>

</body>
</html>