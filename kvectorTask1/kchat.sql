-- this is a sql file that contains sql statements to create kchat database.
-- for tutorials on sql syntax you may want to visit: http://www.w3schools.com/sql/default.asp
-- note here that the semi-colon (;) specifies the end of the statement, so if you want to
-- copy paste the statements here into your mysql command line, make sure you copy the 
-- statement until the semicolon then paste it in the command line.
-- the (--) signifies a comment on this line (no need to copy/paste it)

-- first, lets create our kchat database (easy as toast)
CREATE DATABASE kchat;

-- this statement tells 
USE kchat;

-- now let's create our first table 'Accounts'.
CREATE TABLE accounts 
( -- here we are going to add columns in our Accounts table
	
	-- this is our id for every account, this id will be automatically inserted for us and will 
	-- start from 1 and will increment by one every time we add a new record.
	id int(10) NOT NULL AUTO_INCREMENT, 

	-- this is our name column
	name varchar(60) NOT NULL UNIQUE,
	
	-- this is our email column which will be unique (each account has one email and no
	-- two accounts have the same email)
	email varchar(60) NOT NULL UNIQUE,
	
	-- this is our password account.
	password varchar(60) NOT NULL,
	
	-- id is our primary key (the key that identifies each account and makes accounts 
	-- different)
	PRIMARY KEY(id)
);


-- now creating our second table: messages
CREATE TABLE messages
(
	-- this is our id for every message, this id will be automatically inserted for
	-- us and will start from 1 and will increment by one every time we add a new record.
	id int(10) NOT NULL AUTO_INCREMENT,

	-- this is foreign key telling us the id of the account who sent the message.
	-- you may want to search for the meaning of foreign key and why we use it in databases
	senderID int(10) NOT NULL,

	-- this is foreign key telling us the id of the account who recieved the message.
	recieverID int(10) NOT NULL,

	-- this is the message contents
	content LONGTEXT NOT NULL,

	-- this shows if the message is seen or no
	seen int(1) NOT NULL DEFAULT 0,

	-- this is the date and time of sent message
	sent_at DATETIME NOT NULL,

	-- here we tell mysql that our id is a primary key
	PRIMARY KEY (id),

	-- here we tell mysql what is our foreign keys and what will they reference
	FOREIGN KEY (senderID) REFERENCES accounts(id),
	FOREIGN KEY (recieverID) REFERENCES accounts(id)
);


-- and we are done setting up our database :D 