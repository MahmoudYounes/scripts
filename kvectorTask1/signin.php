<?php

/*
|----------------------------------------------------------------------
| signin.php
|----------------------------------------------------------------------
| this is the php page for signing a user into our system then
| redirecting them to the chat.php page to start chatting.    
| note: that the form is processed in the same page as
| it was written and the url to which we submitted
| the form is this page that we coded in php..
| 
*/

// this is like #include <iostream> for example. it includes a file in here.
require_once("functions.php");


// $_POST is a super global array (an array that is the same for all php pages)
// this variable stores any form data sent through POST method. (ask google to
// stand more).
// if you have not already guessed, there is a variable called $_GET that contains all 
// form data submitted through GET method.
if(isset($_POST['submit']))
{
	//TODO: prepare to call sign in function. function should take username and password
	//      and return true if sign in successful or false if an error occured.

	//recognize this from c/c++ ?
	// if not, search for shorthand if/else in php or shorthand if/else for c++.
	$username = (isset($_POST['username']))? $_POST['username'] : die("bad username.");
	$password = (isset($_POST['password']))? $_POST['password'] : die("bad password.");

	//are there any checks required? why? why not?


	//if signin returned true then redirect to chat.php
	//go to functions.php to implement signin function
	if (signin($username, $password))
	{		
		redirect_to("chat.php");
	}
	else
	{
		echo "<p style=\"text-align:center;\">failed to signin, please retry.</p><br/>";
	}

}


?>

<html>
<head>
<title>kchat - signin</title>
<style>
form
{
	text-align: center;
	width: 100%;
	min-height: 22em;
}
</style>
</head>

<body>
<div>
	<a href="index.php">&lt;&lt; back</a>
</div>
	<!-- 
		- here we created a form, identified the method of submission which is POST and 
	      indentified where to submit the data "signin.php.
	    - why POST? POST hides the data. if we used GET, data would exist in the URL, see
	      testGET.php file for example.
	    - where does POST hide the date? (search topic :-P)
	-->
	<form method="POST"	action="signin.php">
		<label for="username">username</label>
		<input name="username" type="text" required/>
		<br/>
		<label for="password">password</label>
		<input name="password" type="password" required/>
		<br/>
		<br/>
		<input style="margin-left: 5%;" type="submit" name="submit" value="submit">
	</form>
</body>
</html>

<!-- 
	after finishing this page correctly, proceed to chat.php
-->