#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;


int * setupTime(int secfx, string t)
{
	int hrs, mins, secs, msecs;
	
	hrs = stoi(t.substr(0,2));
	mins = stoi(t.substr(3,2));
	secs = stoi(t.substr(6,2));
	msecs = stoi(t.substr(9));
	
	cerr << t << endl;
	if (secfx > 0)
	{
		if (secs - secfx < 0)
		{
			if (mins == 0)
			{
				if (hrs == 0)
				{
					cerr << "error in srt file format.\n";
					exit(1);
				}
				hrs--;
				mins = 59;
			}
			secs = 60 - (secfx - secs);
			mins--;
		}
		else
			secs -= secfx;
	}
	else
	{
		if (secs - secfx > 59)
		{
			if (mins == 59)
			{
				hrs++;
				mins = 0;
			}
			secs -= secfx;
			secs %= 60;
			mins++;
		}
		else
		{
			secs -= secfx;
		}
	}

	int * arr = new int(4);
	arr[0] = hrs;
	arr[1] = mins;
	arr[2] = secs;
	arr[3] = msecs;
	return arr;

}


bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

string tidyUp(int x)
{
	string ret = to_string(x);
	if(x < 10)
	{
		ret = "0" + ret;
	}
	return ret;
}

string tidyUpMS(int x)
{
	string ret = to_string(x);
	if(x < 10)
	{
		ret = "00" + ret;
	}
	else if (x < 100)
	{
		ret = "0" + ret;
	}
	return ret;	
}

int main()
{
	int secFx;
	string fileName;
	string oFileName;
	cout << "enter file name (example.srt): ";
	cin >> fileName;
	oFileName = fileName.substr(0, fileName.length() - 4) + "fixed.srt";

	cout << "enter seconds to subtract (-ve number): ";
	cin >> secFx;

	//TODO: make function that checks file name and number inputs.


	freopen(fileName.c_str(), "r", stdin);
	freopen(oFileName.c_str(), "w", stdout);
	
	int linesCount = 1, xc;
	string lc, srcTime, dstTime, token;	
	string in;
	
	cin >> lc;
	// redundant check. some files contain redundant characters at the begining of the file
	// only one file detected with this case.
	if (lc != "1")
		lc = "1";
	while(1)
	{
		
		cerr << "test: " << lc << ' ' << linesCount << endl;
		if(!is_number(lc) || !(linesCount == stoi(lc)))
		{
			cerr << "finished.";
			return 0;
		}

		cin >> srcTime >> token >> dstTime;
		cout << linesCount << endl;
		int * arr_s = setupTime(secFx, srcTime);
		int * arr_d = setupTime(secFx, dstTime);
		

		cout << tidyUp(arr_s[0]) << ":" << tidyUp(arr_s[1]) << ":" << tidyUp(arr_s[2]) << "," << tidyUpMS(arr_s[3]) 
		     << " --> " << tidyUp(arr_d[0]) << ":" << tidyUp(arr_d[1]) << ":" << tidyUp(arr_d[2]) << "," << tidyUpMS(arr_d[3]) << '\n';

		string x;
		bool test = 0;
		while(getline(cin, x))
		{
			if (is_number(x.substr(0,x.length()-1)))
			{
				lc = x.substr(0,x.length()-1);
				cerr << "lc: " << lc << endl;
				break;
			}
			cout << x;
			
		}
		cout << '\n' << '\n';
		linesCount++;
	}
	return 0;
}